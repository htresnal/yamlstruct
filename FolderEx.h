#ifndef FOLDEREX_CLASSES_H
#define FOLDEREX_CLASSES_H

#include <vector>
#include "FileEx.h"

using namespace std;

class FolderEx : public FileEx {
private:
    string _name;
    shared_ptr<FileEx> _root;
    vector<shared_ptr<FileEx>> _files;
    
public:
    vector<shared_ptr<FileEx>> &getFiles() { return this->_files; }
    const string &getName(){ return this->_name; }
    void setName(const string &newName) { this->_name = newName; }
    void setFiles(const vector<shared_ptr<FileEx>> &newFiles) { this->_files = newFiles; }

    FolderEx(const string &path) : FileEx(path) {
        this->_name = path;
    }
    FolderEx(shared_ptr<FileEx> rootFolder, const string &path) : FileEx(rootFolder, path) {
        this->_root = rootFolder;
        this->_name = path;
    }

    virtual ~FolderEx() = default;
};

#endif