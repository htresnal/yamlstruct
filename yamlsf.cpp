#include <iostream>
#include <string>
#include <filesystem>
#include <fstream>
#include <yaml-cpp/yaml.h>


using namespace std;
namespace fs = filesystem;


struct G_Settings{
    string yamlPath;
    string reconstrPath;

    int parseSettings(int argc, char **argv){
        if (argc < 2 || argc > 3){
            cout << "Usage: ./yamlsf <yaml_path> <reconstruction_path>\n";

            return -2;
        }

        if (string(argv[1]) == "-h"s){
            cout << "Usage: ./yamlsf <yaml_path> <reconstruction_path>\n";

            return 1;
        }

        this->yamlPath = string(argv[1]);
        this->reconstrPath = string(argv[2]);

        if (!fs::exists(this->yamlPath)){
            cout << "Invalid yaml path: " << this->yamlPath << ". No file found.";
            return -3;
        }

        if (!fs::exists(this->reconstrPath)){
            cout << "Invalid reconstruction path: " << this->reconstrPath << ". No folder found.";
            return -4;
        }

        return 0;
    }
} g_settings;

void reconstructYAMLToFolderAndFile(YAML::Node rootNode, const fs::path constrPath);

void reconstructYAMLSequenceToFolderAndFile(YAML::Node rootNode, const fs::path constrPath){
    for(YAML::const_iterator it=rootNode.begin();it!=rootNode.end();++it) {
        switch (it->Type()) {
            case YAML::NodeType::Null:
                break;
            case YAML::NodeType::Scalar:
                {
                    string fileFullPath = ( constrPath / it->as<string>() ).string();
                    ofstream newFile;
                    newFile.open(fileFullPath);

                    if (!newFile.is_open()) {
                        std::cout << ("Unable to create file "+fileFullPath);
                    } else {
                        newFile.close();
                    }
                }
                break;
            case YAML::NodeType::Sequence:
                {
                    reconstructYAMLToFolderAndFile(*it, constrPath);
                }
                break;
            case YAML::NodeType::Map:
                {
                    reconstructYAMLToFolderAndFile(*it, constrPath);
                }
                break;
            case YAML::NodeType::Undefined:
                break;
        }
    }
}

void reconstructYAMLToFolderAndFile(YAML::Node rootNode, const fs::path constrPath){
    if (rootNode.IsSequence()){
        reconstructYAMLSequenceToFolderAndFile(rootNode, constrPath);

        return;
    }

    for(YAML::const_iterator it=rootNode.begin();it!=rootNode.end();++it) {
        switch (it->second.Type()) {
            case YAML::NodeType::Null:
                break;
            case YAML::NodeType::Scalar:
                {
                    string fileFullPath = ( constrPath / it->first.as<string>() ).string();
                    ofstream writtenFile;
                    writtenFile.open(fileFullPath);

                    if (!writtenFile.is_open()) {
                        std::cout << ("Unable to open and/or write to file "+fileFullPath);
                    } else {
                        writtenFile << it->second.as<string>();
                        writtenFile.close();
                    }
                }
                break;
            case YAML::NodeType::Sequence:
                {
                    if ( !filesystem::exists(constrPath / it->first.as<string>()) ) {
                        std::filesystem::create_directory(constrPath / it->first.as<string>());
                    }

                    reconstructYAMLToFolderAndFile(it->second, constrPath / it->first.as<string>());
                }
                break;
            case YAML::NodeType::Map:
                {
                    if ( !filesystem::exists(constrPath / it->first.as<string>()) ) {
                        std::filesystem::create_directory(constrPath / it->first.as<string>());
                    }

                    reconstructYAMLToFolderAndFile(it->second, constrPath / it->first.as<string>());
                }
                break;
            case YAML::NodeType::Undefined:
                break;
        }
    }
}

int main(int argc, char **argv){
    int result = g_settings.parseSettings(argc, argv);
    if (result > 0) return 0;
    if (result < 0) return -1;

    const fs::path yamlPath{g_settings.yamlPath};
    const fs::path reconstrPath{g_settings.reconstrPath};

    YAML::Node rootNode = YAML::LoadFile(yamlPath);

    reconstructYAMLToFolderAndFile(rootNode, reconstrPath);

    return 0;
}
