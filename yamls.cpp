#include <iostream>
#include <fstream>
#include <string>
#include <filesystem>
#include <memory>
#include <map>
#include "FileEx.h"
#include "FolderEx.h"
#include <yaml-cpp/emittermanip.h>
#include <yaml-cpp/node/type.h>
#include <yaml-cpp/yaml.h>


using namespace std;
namespace fs = filesystem; 


struct G_Settings{
    bool filesAsValueMode = false;
    bool skipFilesMode = false;
    bool outputToFile = false;
    string rootPath;
    string outputPath;

    int parseSettings(int argc, char **argv){
        if (argc < 2 || argc > 4){
            cout << "Usage: ./yamls [-fav, -sf] <path> <output_path>\n";
            cout << "-fav -- Don't read file content. Use file names as values, instead.\n";
            cout << "-sf  -- Skip files. Look for folder structure only.\n";

            return -2;
        }

        if (string(argv[1]) == "-h"s){
            cout << "Usage: ./yamls [-fav, -sf] <path> <output_path>\n";
            cout << "-fav -- Don't read file content. Use file names as values, instead.\n";
            cout << "-sf  -- Skip files. Look for folder structure only.\n";

            return 1;
        } else if (string(argv[1]) == "-fav"s) {
            this->filesAsValueMode = true;
        } else if (string(argv[1]) == "-sf"s){
            this->skipFilesMode = true;
        }

        if (this->filesAsValueMode || this->skipFilesMode){
                this->rootPath = string(argv[2]);
                this->outputPath = string(argv[3]);
        } else {
                this->rootPath = string(argv[1]);
                this->outputPath = string(argv[2]);
        }

        if (!fs::exists(this->rootPath)){
            cout << "Invalid root path: " << this->rootPath << ". No file or folder found.";
            return -3;
        }

        this->outputToFile = !(this->outputPath.empty());

        return 0;
    }
} g_settings;


void constructFolderEx(shared_ptr<FolderEx> parentFolder, const fs::path &elemPath){
    for(const auto& entry : fs::directory_iterator(elemPath)) {
        const fs::path fullPath = entry.path();
        
        if(fs::is_directory(entry.status())) {
            shared_ptr<FolderEx> newFolder = make_shared<FolderEx>(fullPath);
            newFolder->setName(fullPath.filename().string());

            constructFolderEx(newFolder, fullPath);

            parentFolder->getFiles().push_back(newFolder);
        } else {
            if (g_settings.skipFilesMode) continue;

            shared_ptr<FileEx> newFile = make_shared<FileEx>(parentFolder, fullPath);

            parentFolder->getFiles().push_back(newFile);
        }
    }

    return;
}

YAML::Node getYAMLNodeFromFolderEx(YAML::Node &rootFolderNode, shared_ptr<FolderEx> parentFolder){
    rootFolderNode[parentFolder->getName()] = YAML::Node(YAML::NodeType::Map);
    YAML::Node newMapNode = rootFolderNode[parentFolder->getName()];

    for (const auto &entry : parentFolder->getFiles()){
        if (auto folder = dynamic_pointer_cast<FolderEx>(entry)) {
            newMapNode[folder->getName()] = getYAMLNodeFromFolderEx(newMapNode, folder);
        } else if (auto file = dynamic_pointer_cast<FileEx>(entry)){
            if (g_settings.skipFilesMode) continue;

            fs::path filepath = file->getPath();
            ifstream fileToReadForContent(filepath.string());
            if (!fileToReadForContent){
                newMapNode[filepath.filename().string()] = YAML::Load("{}");
            } else {
                string fileContent(
                    ( istreambuf_iterator<char>(fileToReadForContent) ), istreambuf_iterator<char>()
                );

                fileToReadForContent.close();

                YAML::Node valueNode = YAML::Load("{}");
                valueNode = fileContent;
                newMapNode[filepath.filename().string()] = valueNode;
            }
        }
    }

    return newMapNode;
}

YAML::Node getYAMLNodeFromFolderExByFAV(YAML::Node &rootFolderNode, shared_ptr<FolderEx> parentFolder){
    rootFolderNode[parentFolder->getName()] = YAML::Node(YAML::NodeType::Sequence);
    YAML::Node newSeqNode = rootFolderNode[parentFolder->getName()];

    for (const auto &entry : parentFolder->getFiles()){
        if (auto folder = dynamic_pointer_cast<FolderEx>(entry)) {
            YAML::Node newFolderNode = YAML::Load("{}");
            newFolderNode[folder->getName()] = getYAMLNodeFromFolderExByFAV(newFolderNode, folder);

            newSeqNode.push_back(newFolderNode);
        } else if (auto file = dynamic_pointer_cast<FileEx>(entry)){
            fs::path filepath = file->getPath();

            newSeqNode.push_back(filepath.filename().string());
        }
    }

    return newSeqNode;
}

int main(int argc, char **argv){
    int result = g_settings.parseSettings(argc, argv);
    if (result > 0) return 0;
    if (result < 0) return -1;

    const fs::path rootPath{g_settings.rootPath};

    if(!fs::exists(rootPath) || !fs::is_directory(rootPath)) {
        cout << rootPath.filename().string() << " is not a valid directory.\n";

        return -1;
    }

    shared_ptr<FolderEx> rootFolder = make_shared<FolderEx>(rootPath);
    constructFolderEx(rootFolder, rootPath);

    YAML::Emitter out;

    YAML::Node rootFolderNode = YAML::Load("{}");

    if (g_settings.filesAsValueMode){
        getYAMLNodeFromFolderExByFAV(rootFolderNode, rootFolder);
    } else {
        getYAMLNodeFromFolderEx(rootFolderNode, rootFolder);
    }

    string rootKey;

    for(YAML::const_iterator it=rootFolderNode.begin();it!=rootFolderNode.end();++it) {
        rootKey = it->first.as<string>();
    }

    out << rootFolderNode[rootKey];

    if (g_settings.outputToFile){
        ofstream fout;

        fout.open(g_settings.outputPath);

        if (!fout.is_open()){
            cout << "Unable to write to file: " << g_settings.outputPath << '\n';
        } else {
            fout << out.c_str() << '\n';
            fout.close();
        }
    } else {
        cout << out.c_str() << '\n';
    }

    return 0;
}
