#ifndef FILEEX_CLASSES_H
#define FILEEX_CLASSES_H

#include <string>
#include <memory>

using namespace std;

class FileEx {
private:
    string _path;
    shared_ptr<FileEx> _root;
    
public:
    string getPath() { return _path; }

    FileEx(const string &path) {
        this->_path = path;
    }
    FileEx(shared_ptr<FileEx> rootFolder, const string &path) {
        this->_root = rootFolder;
        this->_path = path;
    }

    virtual ~FileEx() = default;
};

#endif