# YamlStruct
https://gitlab.com/htresnal/yamlstruct

*version 0.1a*

_What is this?_ <br/>
**yamls** - This application creates a YAML representation of a specified folder's structure.<br/>
**yamlsf** - This application takes a YAML representation of a folder's structure, and recreates it inside of a specified folder. Together with files and their content.

## Requirements

- C++17 compiler
- make
- jbeder's yaml-cpp https://github.com/jbeder/yaml-cpp

## Prerequisites

```bash
# Debian:
sudo apt-get install build-essential libyaml-cpp-dev

# Arch:
sudo pacman -S gcc yaml-cpp

# Manjaro:
sudo pamac install gcc yaml-cpp
```

## How to install

```bash
# Build and install

- make
- make install


# Remove

- make uninstall
```

## Usage

### YAMLS
```bash
yamls [-fav, -sf] <path> <output_path>
```

\<path\> - Path to the directory you want to create a YAML structure of.<br/>
<output_path> - Path to save the result YAML at.

**Modes:** <br/>
Default - Folders are copied as-is. File names are keys, and file content is the value.
```yaml
folder:{
    file.txt: "\n888\n\n123\n"
}
```
***
Skip files[-sf] - Read folders only.
```yaml
folder: {}
```
***
Files as values[-fav] - Do not read file content. Put file names into arrays, instead.
```yaml
{
    folder: [
        "file.txt"
    ]
}
```

### YAMLSF
```bash
yamlsf <yaml_path> <reconstruction_path>
```

<yaml_path> - Path to the YAML file you want to create a representation of.<br/>
<reconstruction_path> - Directory you want to reconstruct the folder and file structure at.

## Integrations

https://gitlab.com/htresnal/yamlstruct-dolphin-integration - YamlStruct integration with KDE Dolphin file manager.
