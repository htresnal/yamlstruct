#/bin/bash


echo " "
echo "### YAMLSTRUCT TEST"
echo "# FOLDER->YAML"
echo "--- READING"
yamls ./tests/testfolder/ ./tests/files.yaml
echo "--- WRITING"
mkdir -p ./tests/temp

yamlsf ./tests/files.yaml ./tests/temp/

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

echo "# FOLDER->YAML: SKIP FILES"
echo "--- READING"
yamls -sf ./tests/testfolder/ ./tests/files-sf.yaml
echo "--- WRITING"
mkdir -p ./tests/temp-sf
yamlsf ./tests/files-sf.yaml ./tests/temp-sf/

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

echo "# FOLDER->YAML: FILES AS VALUES"
echo "--- READING"
yamls -fav ./tests/testfolder/ ./tests/files-fav.yaml
echo "--- WRITING"
mkdir -p ./tests/temp-fav
yamlsf ./tests/files-fav.yaml ./tests/temp-fav/
