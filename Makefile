CC=c++
CFLAGS=-std=c++17 -o2
LDFLAGS=-lyaml-cpp
EXECUTABLE_IN=yamls
EXECUTABLE_OUT=yamlsf

all: $(EXECUTABLE_IN) $(EXECUTABLE_OUT)

$(EXECUTABLE_IN):
	$(CC) FileEx.h FolderEx.h yamls.cpp -o $(EXECUTABLE_IN) $(LDFLAGS)

$(EXECUTABLE_IN).o:
	$(CC) $(CFLAGS) yamls.cpp

$(EXECUTABLE_OUT):
	$(CC) FileEx.h FolderEx.h yamlsf.cpp -o $(EXECUTABLE_OUT) $(LDFLAGS)

$(EXECUTABLE_OUT).o:
	$(CC) $(CFLAGS) yamlsf.cpp

clean:
	rm ./$(EXECUTABLE_IN)
	rm ./$(EXECUTABLE_OUT)

install:
	sudo cp ./$(EXECUTABLE_IN) /usr/local/bin/$(EXECUTABLE_IN)
	sudo cp ./$(EXECUTABLE_OUT) /usr/local/bin/$(EXECUTABLE_OUT)

uninstall:
	sudo rm -f /usr/local/bin/$(EXECUTABLE_IN)
	sudo rm -f /usr/local/bin/$(EXECUTABLE_OUT)
